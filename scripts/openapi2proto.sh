#!/bin/bash

# Generate .proto files from OpenAPI3 files in api/openapi

go install github.com/google/gnostic@latest
go install github.com/googleapis/gnostic-grpc@latest

sudo cp ~/go/bin/gnostic /bin
sudo cp ~/go/bin/gnostic-grpc /bin

for file in ../api/openapi/*; do
    gnostic --grpc-out=../api/protobuf ../api/openapi/"$(basename "$file")" --messages-out=/dev/null
    echo "$(basename "$file")"
done